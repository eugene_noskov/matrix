package org.eugene.matrix;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



@RunWith(MockitoJUnitRunner.class)
public class MatrixTest extends Matrix{
    private List<Point> redPoints;
    List<Point> greenPoints;
    List<Point> bluePoints;
    int[][] matrix;


    @Before
    public void initTest() {
        redPoints = new ArrayList<>();
        redPoints.add(new Point(0, 0, Color.RED));
        redPoints.add(new Point(1, 1, Color.RED));
        redPoints.add(new Point(2, 2, Color.RED));

        greenPoints = new ArrayList<>();
        greenPoints.add(new Point(4, 0, Color.GREEN));
        greenPoints.add(new Point(4, 1, Color.GREEN));
        greenPoints.add(new Point(4, 2, Color.GREEN));

        bluePoints = new ArrayList<>();
        bluePoints.add(new Point(2, 3, Color.BLUE));
        bluePoints.add(new Point(2, 4, Color.BLUE));

         matrix= new int[5][5];
        setPoints(matrix, redPoints);
        setPoints(matrix, greenPoints);
        setPoints(matrix, bluePoints);
    }

    @Test
    public void testSettingSameColor() {
        int[][] correctMatrix = new int[5][5];
        setPoints(correctMatrix, redPoints);
        setPoints(correctMatrix, greenPoints);
        setPoints(correctMatrix, bluePoints);

        int[][] filled = Matrix.fill(matrix, 0, 0, Color.RED.getId());
        assertTrue(matricesAreEqual(correctMatrix, filled));
    }

    @Test
    public void testSettingGreenColorToRedLine() {
        int[][] filled = Matrix.fill(matrix, 0, 0, Color.GREEN.getId());

        List<Point> redRepaintToGreenPoints = new ArrayList<Point>();
        redRepaintToGreenPoints.add(new Point(0, 0, Color.GREEN));
        redRepaintToGreenPoints.add(new Point(1, 1, Color.GREEN));
        redRepaintToGreenPoints.add(new Point(2, 2, Color.GREEN));

        int[][] correctMatrix = new int[5][5];
        setPoints(correctMatrix, redRepaintToGreenPoints);
        setPoints(correctMatrix, greenPoints);
        setPoints(correctMatrix, bluePoints);

        assertTrue(matricesAreEqual(correctMatrix, filled));
    }

    @Test
    public void testSettingBlueColorToRedLine() {
        int[][] filled = Matrix.fill(matrix, 2, 4, Color.GREEN.getId());

        List<Point> blueRepaintToGreenPoints = new ArrayList<>();
        blueRepaintToGreenPoints.add(new Point(2, 3, Color.GREEN));
        blueRepaintToGreenPoints.add(new Point(2, 4, Color.GREEN));

        int[][] correctMatrix = new int[5][5];
        setPoints(correctMatrix, redPoints);
        setPoints(correctMatrix, greenPoints);
        setPoints(correctMatrix, blueRepaintToGreenPoints);

        assertTrue(matricesAreEqual(correctMatrix, filled));
    }

    @Test
    public void testSettingGreenColorToWhiteCells() {
        int[][] correctMatrix = new int[5][5];
        correctMatrix = fillMatrixToColor(correctMatrix, Color.GREEN);
        setPoints(correctMatrix, redPoints);
        setPoints(correctMatrix, bluePoints);

        int[][] filled = Matrix.fill(correctMatrix, 0, 1, Color.GREEN.getId());

        assertTrue(matricesAreEqual(correctMatrix, filled));
    }

    @Test
    public void testLimit1() {
        Point point = new Point(0, 0, Color.BLUE);
        int[][] matrix = new int[5][5];

        AreaForSearching neighborsAreaForSearching = Matrix.calculateAreaForSearching(point, matrix);

        assertEquals(0, neighborsAreaForSearching.getX0());
        assertEquals(1, neighborsAreaForSearching.getX1());
        assertEquals(0, neighborsAreaForSearching.getY0());
        assertEquals(1, neighborsAreaForSearching.getY1());
    }
    @Test
    public void testLimit2() {
        Point point = new Point(4, 0, Color.BLUE);
        int[][] matrix = new int[5][5];

        AreaForSearching neighborsAreaForSearching = Matrix.calculateAreaForSearching(point, matrix);

        assertEquals(3, neighborsAreaForSearching.getX0());
        assertEquals(4, neighborsAreaForSearching.getX1());
        assertEquals(0, neighborsAreaForSearching.getY0());
        assertEquals(1, neighborsAreaForSearching.getY1());
    }

    @Test
    public void testLimit3() {
        Point point = new Point(0, 4, Color.BLUE);
        int[][] matrix = new int[5][5];

        AreaForSearching neighborsAreaForSearching = Matrix.calculateAreaForSearching(point, matrix);

        assertEquals(0, neighborsAreaForSearching.getX0());
        assertEquals(1, neighborsAreaForSearching.getX1());
        assertEquals(3, neighborsAreaForSearching.getY0());
        assertEquals(4, neighborsAreaForSearching.getY1());
    }

    @Test
    public void testLimit4() {
        Point point = new Point(4, 4, Color.BLUE);
        int[][] matrix = new int[5][5];

        AreaForSearching neighborsAreaForSearching = Matrix.calculateAreaForSearching(point, matrix);

        assertEquals(3, neighborsAreaForSearching.getX0());
        assertEquals(4, neighborsAreaForSearching.getX1());
        assertEquals(3, neighborsAreaForSearching.getY0());
        assertEquals(4, neighborsAreaForSearching.getY1());
    }

    @Test
    public void testLimit5() {
        Point point = new Point(3, 2, Color.BLUE);
        int[][] matrix = new int[5][5];

        AreaForSearching neighborsAreaForSearching = Matrix.calculateAreaForSearching(point, matrix);

        assertEquals(2, neighborsAreaForSearching.getX0());
        assertEquals(4, neighborsAreaForSearching.getX1());
        assertEquals(1, neighborsAreaForSearching.getY0());
        assertEquals(3, neighborsAreaForSearching.getY1());
    }

    private int[][] fillMatrixToColor(int[][] matrix, Color color) {
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                matrix[x][y] = color.getId();
            }
        }
        return matrix;
    }

    private boolean matricesAreEqual(int[][] correctMatrix, int[][] checkingMatrix) {
        for (int x = 0; x < correctMatrix.length; x++) {
            for (int y = 0; y < correctMatrix[0].length; y++) {
                if (correctMatrix[x][y] != checkingMatrix[x][y]){
                    return false;
                }
            }
        }
        return true;
    }

    public void setPoints(int[][] matrix, List<Point> points){
        for (Point point : points) {
            matrix[point.getX()][point.getY()] = point.getColor().getId();
        }
    }
}






