package org.eugene.matrix;

public enum Color {
    EMPTY(0),
    RED(1),
    GREEN(2),
    BLUE(3);

    private int colorId;

    Color(int i) {
        this.colorId = i;
    }
    public int getId() {
        return colorId;
    }

    static public Color findById(int id) {
        for (Color value : Color.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return Color.EMPTY;
    }

    public int getColorId() {
        return colorId;
    }
}
