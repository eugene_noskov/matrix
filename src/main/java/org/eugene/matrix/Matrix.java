package org.eugene.matrix;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Matrix {

    public static int[][] fill(int[][] matrix, int x, int y, int newColor){
        int lookForColor = matrix[x][y];
        boolean colorIsTheSame = lookForColor == newColor;
        if (colorIsTheSame) {
            return matrix;
        }
        Point point = new Point(x, y, Color.findById(lookForColor));
        searchForPointsWithColorAndRepaint(matrix, point, newColor);
        return matrix;
    }

    private static List<Point> searchForPointsWithColorAndRepaint(int[][] matrix, Point point, int newColor) {
        List<Point> pointsToPaint = new ArrayList<>();

        Color colorToLookFor = point.getColor();

        AreaForSearching areaForSearching = calculateAreaForSearching(point, matrix);
        List<Point> neighboursForProcessing = new ArrayList<>();
        for (int x = areaForSearching.getX0(); x <= areaForSearching.getX1(); x++) {
            for (int y = areaForSearching.getY0(); y <= areaForSearching.getY1(); y++) {
                if (matrix[x][y] == colorToLookFor.getId()) {
                    neighboursForProcessing.add(new Point(x, y, colorToLookFor));
                    matrix[x][y] = newColor;
                }
            }
        }

        for (Point neighbour : neighboursForProcessing) {
            pointsToPaint.add(neighbour);
            if (neighbour.equals(point)) {
                continue;
            }
            List<Point> pointsToRepaint = searchForPointsWithColorAndRepaint(matrix, neighbour, newColor);
            pointsToPaint.addAll(pointsToRepaint);
        }
        return pointsToPaint;
    }

    protected static AreaForSearching calculateAreaForSearching(Point point, int[][] matrix) {
        AreaForSearching coordinateLimits = new AreaForSearching();
        int length = matrix.length - 1;
        int height = matrix[0].length - 1;

        coordinateLimits.setX0(Math.max(0, point.getX() - 1));
        coordinateLimits.setX1(Math.min(point.getX() + 1, length));
        coordinateLimits.setY0(Math.max(0, point.getY() - 1));
        coordinateLimits.setY1(Math.min(point.getY() + 1, height));

        return coordinateLimits;
    }
}
