package org.eugene.matrix;

import lombok.Data;

@Data
public class AreaForSearching {
    private int x0 = -1;
    private int x1 = -1;
    private int y0 = -1;
    private int y1 = -1;
}
